#20203322冷骏
#2021.05.28
#FileName：客户端.py
import socket
import os
import base64
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 8005))
str1 = input("请输入要传输的文件名：")
s.sendall(str1.encode())
os.chdir(r"E:\client\download")
file = open(str1, 'r')
text = file.read()
text = text.encode('utf-8')
encode_text = base64.b32encode(text)
s.sendall(encode_text)
file.close()
data = s.recv(1024)
print("来自 ('127.0.0.1') 的信息：", data.decode())
s.sendall("收到".encode())
name = s.recv(1024)
print("来自 ('127.0.0.1') 的文件：", name.decode())
data = s.recv(1024)
f = open("reply.txt", "w")
f.write(data.decode())
f.close()
print("文件内容已加密发送")
s.sendall("已成功接收，中断连接！".encode())
s.close()
